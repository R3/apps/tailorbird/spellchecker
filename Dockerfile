FROM bash AS bash
FROM git-r3lab.uni.lu:4567/r3/apps/tailorbird/spellchecker:aspell-0.60.8 as aspell
# use proprietary image to test

ADD ./spellchecker .
CMD bash test_spell_check.sh