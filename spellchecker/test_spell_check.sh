#!/bin/bash
set -e


echo "Spell checking"
spell_check_code=0

version=`aspell --version`
echo $version

aspellOptions=`cat aspellOptions.txt`
echo "using the following aspell command:"
echo $aspellOptions
echo 

for x in $(find spellCheck/ -name '*.md');
do	
	cmdLines=`cat $x | aspell $aspellOptions list`
	echo $cmdLines
	# note: cmdLines is the command
	# $( . ): executes the command
	# $(( . )): converts the result from wc -l into a number
#	lines=$(($(eval $cmdLines)))
#	if [ $lines -gt 0 ]
#	then
#		echo $x:
#		cat $x | aspell $aspellOptions list
#		spell_check_code=1
#		echo 
#	fi
# we need to check stderr - in case something really bad happens there will be output there
#	cmdLines="cat errors.txt | wc -l"
#	lines=$(($(eval $cmdLines)))
#	if [ $lines -gt 0 ]
#	then
#		cat errors.txt
#		spell_check_code=1
#	fi
done

if [ $spell_check_code -gt 0 ]
then
	echo "Spell check found some problems. Either fix them or add exceptions to the dictionary file: .aspell.en.pws"
fi

#rm errors.txt
exit $spell_check_code
